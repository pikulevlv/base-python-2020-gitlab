from flask import Flask,request, render_template
from flask_migrate import Migrate

import config
from views import product_app
from models import db

app = Flask(__name__)
app.config.update(
    SERVER_NAME="localhost:5000",
    SQLALCHEMY_DATABASE_URI=config.SQLALCHEMY_DATABASE_URI,
)
app.register_blueprint(product_app, url_prefix="/products")

db.init_app(app)
migrate = Migrate(app, db)

@app.route("/", methods=['POST', 'GET']) # обработчик индекса "/"
def index():
    name = "World"
    if request.method == 'POST':
        name = request.form.get('name', default='WORLD')
    return render_template("index.html", name="World")

# app.add_url_rule("/", "index", index) # устаревшая альтернатива декоратору @app.route
# app.add_url_rule("/", view_func=index)

@app.route("/hello/")
@app.route("/hello/<string:name>/")
def hello(name=None):
    if name is None:
        name = 'WORLD'
    return f"<h1>Hello, {name}!</h1>"

# @app.route("/post/<post_id>/")
# def alt_show_post(post_id):
#     post_id > 0
#     return f"Post (str) {post_id}"

@app.route("/post/<int:post_id>/")
def show_post(post_id):
    return f"Post (int) {post_id}"

