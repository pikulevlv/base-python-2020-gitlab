from .database import db
from .product import Product

# все что мы импортируем:
__all__ = [
    "db",
    "Product",
]
# если не перечислено в __all__,
# то при from models import * не импортируется